#include <string>
#include <iostream>
#include <chrono>

#include "timer.h"
namespace timer{
static std::chrono::time_point<std::chrono::high_resolution_clock> t1;
void start(){
    t1 = std::chrono::high_resolution_clock::now();
}
void time(std::string msg){
    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    std::cout << msg << ": " << duration/1000000.0 << "s" << std::endl;
}
}

