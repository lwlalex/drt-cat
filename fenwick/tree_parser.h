#ifndef TREE_PARSER_H_
#define TREE_PARSER_H_

#include <iostream>
#include <string>
#include <vector>
#include "leaf_group.h"

std::vector<std::vector<struct leaf_group>> parse_trees(char *file1, char *file2);
int read_leaf(std::string &s, int &i);
int read_leaf_new(char *c);

#endif
