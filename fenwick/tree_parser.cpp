#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "tree_parser.h"
#include "leaf_group.h"
#include "timer.h"

std::vector<std::vector<struct leaf_group>> parse_trees(char *file1, char *file2){

    // read files
    std::string t_goal, t_start;
    std::ifstream file_reader(file1);
    getline(file_reader, t_goal, ';');

    file_reader = std::ifstream(file2);
    getline(file_reader, t_start, ';');

    // go through t_goal, build label map, get #leaves
    // 1st pass, get height and #leaves
    int t_goal_height = 0;
    int num_leaves = 1;
    for(char c: t_goal){
        switch(c){
            case '(':
                t_goal_height++;
                break;
            case ',':
                num_leaves++;
                break;
        }
    }

    // 2nd pass, build label map
    int label_map[num_leaves]; // 0-based
    int current_level = 0;
    for(int i = 1; i < t_goal.length(); i++){
        char c = t_goal[i];
        if('0' <= c && c <= '9'){ // read number, fill map
            int leaf_val = read_leaf(t_goal, i);
            label_map[leaf_val-1] = t_goal_height - current_level;
        }
        else if(c == '('){ // going down
            current_level++;
        }
        else if(c == ')'){ // going up
            current_level--;
        }
    }

    // go through t_start, extract leafs and store in t_start_leaves
    // position [i][j] represents the j-th leaf on the i-th level
    std::vector<std::vector<int>> t_start_leaves;
    t_start_leaves.push_back(std::vector<int>());
    current_level = 0;

    for(int i = 1; i < t_start.length(); i++){
        char c = t_start[i];
        if('0' <= c && c <= '9'){ // read number, fill map
            int leaf_val = read_leaf(t_start, i);
            //std::cout << leaf_val << std::endl;
            t_start_leaves[current_level].push_back(leaf_val);
        }
        else if(c == '('){ // going down
            current_level++;
            t_start_leaves.push_back(std::vector<int>());
        }
        else if(c == ')'){ // going up
            current_level--;
        }
    }

    // map and sort each subarray
    for(int i = 0; i < t_start_leaves.size(); i++){
        for(int j = 0; j < t_start_leaves[i].size(); j++){
            t_start_leaves[i][j] = label_map[t_start_leaves[i][j]-1];
        }
        std::sort(t_start_leaves[i].begin(), t_start_leaves[i].end());
    }

    // build leaf group vector
    std::vector<std::vector<struct leaf_group>> leaf_groups;
    int current_leaf_val = -1;
    int new_leaf_val;
    for(int i = 0; i < t_start_leaves.size(); i++){
        leaf_groups.push_back(std::vector<struct leaf_group>());
        for(int j = 0; j < t_start_leaves[i].size(); j++){
            new_leaf_val = t_start_leaves[i][j];
            if(new_leaf_val != current_leaf_val){
                current_leaf_val = new_leaf_val;
                struct leaf_group new_leaf_group = {
                    new_leaf_val, // leaf_val
                    1 //num_leaves
                };
                leaf_groups[i].push_back(new_leaf_group);
            }
            else{
                leaf_groups[i].back().num_leaves++;
            }
        }
        current_leaf_val = -1;
    }
    return leaf_groups;
}

int read_leaf(std::string &s, int &i){
    int val = s[i] - '0';
    while('0' <= s[i+1] && s[i+1] <= '9'){
        val *= 10;
        val += s[i+1] - '0';
        i++;
    }
    return val;
}
