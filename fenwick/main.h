#ifndef GENCAT_H_
#define GENCAT_H_

typedef unsigned long long int ull;

ull triplet_distance(char *file1, char *file2);
ull nc2(int n);

#endif
