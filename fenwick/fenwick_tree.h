#ifndef FENWICK_TREE_H_
#define FENWICK_TREE_H_
#include <vector>
class fenwick_tree {
    std::vector<int> v_;

    public:

    fenwick_tree(int N) : v_(N+1) {}

    void add(int k, int val) {
        for (int i=k, e=v_.size(); i<e; i += -i&i)
            v_[i] += val;
    }

    int leq(int k) const {
        int r=0;
        for (int i=k; i>0; i -= -i&i)
            r += v_[i];
        return r;
    }
	int le(int k) const {
		return leq(k-1);
	}
	int eq(int i){
	return leq(i)-le(i);
	}
	int ge(int i){
		return leq(v_.size()-1) - leq(i);
	}
	int geq(int i){
		return leq(v_.size()-1) - le(i);
	}
};

#endif
