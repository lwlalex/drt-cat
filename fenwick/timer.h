#ifndef TIMER_H_
#define TIMER_H_

#include <string>
#include <iostream>
#include <chrono>

namespace timer{
void start();
void time(std::string msg);
}
#endif
