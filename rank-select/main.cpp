#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "main.h"
#include "tree_parser.h"
#include "leaf_group.h"
#include "counter_tree.h"
#include "timer.h"


int main(int argc, char* argv[]){
    timer::start();
    ull d_rt = triplet_distance(argv[1], argv[2]);
    std::cout << "Triplet Distance: " << d_rt << std::endl;
    timer::time("Time");
    return 0;
}
ull triplet_distance(char *file1, char *file2){
    std::vector<std::vector<struct leaf_group>> leaf_groups = parse_trees(file1, file2);

    int num_leaf_groups = 0;
    for(int i = 0; i < leaf_groups.size(); i++){
        num_leaf_groups += leaf_groups[i].size();
    }
    int num_leaf_vals = 0;
    for(int i = 0; i < leaf_groups.size(); i++){
        for(int j = 0; j < leaf_groups[i].size(); j++){
            if(leaf_groups[i][j].leaf_val > num_leaf_vals)
                num_leaf_vals = leaf_groups[i][j].leaf_val;
        }
    }
    /* std::cout << num_leaf_vals << std::endl; */
    counter_node* root = build_counter_tree(num_leaf_vals);
    //timer::time("End CT");
    /*
    struct leaf_group g1 = {1,2};
    struct leaf_group g2 = {3,1};
    struct leaf_group g3 = {8,15};
    struct leaf_group g4 = {3,1};
    std::cout << "Inserting groups..." << std::endl;
    insert_leaf_group(root, g1);
    insert_leaf_group(root, g2);
    insert_leaf_group(root, g3);
    insert_leaf_group(root, g4);

    int le = 0; int eq = 0; int ge = 0;
    query(root, 3, le, eq, ge);
    std::cout << "query(3): " << le << "," << eq << "," << ge << std::endl;
    le = 0; eq = 0; ge = 0;
    query(root, 5, le, eq, ge);
    std::cout << "query(5): " << le << "," << eq << "," << ge << std::endl;
    le = 0; eq = 0; ge = 0;
    query(root, 1, le, eq, ge);
    std::cout << "query(1): " << le << "," << eq << "," << ge << std::endl;
    le = 0; eq = 0; ge = 0;
    query(root, 12, le, eq, ge);
    std::cout << "query(12): " << le << "," << eq << "," << ge << std::endl;
    */
    /*
    std::vector<int> A();
    std::vector<int> B();
    std::vector<int> C();
    std::vector<int> D();
    std::vector<int> G();
    std::vector<int> L();
    std::vector<ull> P();
    std::vector<ull> Q();
    */
    int L_cumul[num_leaf_vals] = {};
    struct leaf_group g;

    ull d_rt = 0;
    for(int i = leaf_groups.size() - 1; i >= 0; i--){
        //if(i==0)timer::time("DRT 1");
        int A[leaf_groups[i].size()] = {};
        int B[leaf_groups[i].size()] = {};
        int C[leaf_groups[i].size()] = {};
        int D[leaf_groups[i].size()] = {};
        int G[leaf_groups[i].size()] = {};
        int L[leaf_groups[i].size()] = {};
        ull P[leaf_groups[i].size()] = {};
        ull Q[leaf_groups[i].size()] = {};

        //if(i==0)timer::time("DRT 2");
        // 1st pass through subarray, get D, G vals
        for(int j = 0; j < leaf_groups[i].size(); j++){
            g = leaf_groups[i][j];
            G[j] = g.num_leaves;
            if(j == 0){ // leftmost group
                for(int k = 1; k < leaf_groups[i].size(); k++){
                    D[0] += leaf_groups[i][k].num_leaves;
                }
            }else{
                D[j] = D[j-1] - G[j];
            }
        }

        //if(i==0)timer::time("DRT 3");
        // 2nd pass, get L vals
        for(int j = 0; j < leaf_groups[i].size(); j++){
            g = leaf_groups[i][j];
            L[j] = L_cumul[g.leaf_val-1];
            L_cumul[g.leaf_val-1] += g.num_leaves;
        }


        //if(i==0)timer::time("DRT 4");
        // 3rd pass, get P vals
        for(int j = 0; j < leaf_groups[i].size(); j++){
            if(j == 0){ // leftmost
                for(int k = 1; k < leaf_groups[i].size(); k++){
                    P[0] += 1LL * L[k] * G[k];
                }
            }else{
                P[j] = P[j-1] - (1LL * L[j] * G[j]);
            }
        }

        //if(i==0)timer::time("DRT 5");
        // 4th pass, get Q vals
        g = leaf_groups[i][0];
        ull Q1 = 0;
        ull Q2 = 0;
        int Q2_C_count = 0;
        int Q2_G_count = 0;

        Q1 = g.num_leaves * query_ge(root, g.leaf_val);
        Q2_G_count = g.num_leaves;

        for(int j = 1; j < leaf_groups[i].size(); j++){
            Q2_C_count = query_geq(root, leaf_groups[i][j].leaf_val);
            Q2 = Q2_C_count * Q2_G_count;
            Q[j] = Q1 - Q2;
            Q1 += leaf_groups[i][j].num_leaves * query_ge(root, leaf_groups[i][j].leaf_val);
            Q2_G_count += leaf_groups[i][j].num_leaves;
        }

        //if(i==0)timer::time("DRT 6");
        // 5th pass, get A, B, C vals
        for(int j = 0; j < leaf_groups[i].size(); j++){
           query(root, leaf_groups[i][j].leaf_val, A[j], B[j], C[j]);
		   /* A[j]=query_le(root,leaf_groups[i][j].leaf_val); */
		   /* B[j]=query_eq(root,leaf_groups[i][j].leaf_val); */
		   /* C[j]=query_ge(root,leaf_groups[i][j].leaf_val); */
           insert_leaf_group(root, leaf_groups[i][j]);
        }


        /*
        std::cout << "A: "; for(int i : A){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "B: "; for(int i : B){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "C: "; for(int i : C){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "D: "; for(int i : D){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "G: "; for(int i : G){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "L: "; for(int i : L){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "P: "; for(ull i : P){std::cout << i << " ";} std::cout << std::endl;
        std::cout << "Q: "; for(ull i : Q){std::cout << i << " ";} std::cout << std::endl;
        */

        //if(i==0)timer::time("DRT 7");
        // compute d_rt
        for(int j = 0; j < leaf_groups[i].size(); j++){
            // Table 2
            d_rt +=
                1LL * G[j] * (
                    1LL * A[j] * (
                        B[j] + C[j] + D[j]
                    )
                    + nc2(B[j])
                    + 1LL * B[j] * (
                         C[j] + D[j]
                    )
                    + nc2(C[j]) - P[j] + Q[j]
                )
                + nc2(G[j]) * (C[j] + D[j]);
        }
        //if(i==0)timer::time("DRT 8");
    }

    return d_rt;
}

ull nc2(int n){
    if(n<=1)
        return 0;
    if(n % 2 == 0)
        return 1LL * (n/2) * (n-1);
    else
        return 1LL * n * ((n-1)/2);
}
