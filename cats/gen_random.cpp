#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <algorithm>

int main(int argc, char* argv[]) {

	if(argc < 4){
		std::cout << "Usage: gen_random [num_pairs] [num_leaves] [p1] [p2]" << std::endl;
		return 0;
	}

	srand (time(NULL));

	int num_trees = std::atoi(argv[1]);
        if(num_trees > 100) num_trees = 100;
	int num_leaves = std::atoi(argv[2]);
	double p1 = std::atof(argv[3]);
    double p2;
    if(argc>=4)
        p2 = std::atof(argv[4]);
    else
        p2 = p1;

	for(int i = 1; i <= num_trees; i++){

		std::string str_a("(1,");
		int num_branch = 1;
		int rand_num;
		for(int j = 1; j < num_leaves; j++){
			 rand_num = rand() % 100;
			if(rand_num < p1*100){ // stay at branch
				str_a.append(std::to_string(j+1)+",");
			}else{ // make new branch
				str_a.append("("+std::to_string(j+1)+",");
				num_branch++;
			}
		}

		str_a.pop_back();
		for(int j=0; j<num_branch; j++)
			str_a.append(")");
		str_a.append(";");

		int label_map[num_leaves];
		for(int j=0; j<num_leaves; j++)
			label_map[j] = j+1;
		std::random_shuffle(&label_map[0], &label_map[num_leaves]);

		std::string str_b("(" + std::to_string(label_map[0]) + ",");
		num_branch = 1;
		for(int j = 1; j < num_leaves; j++){
			 rand_num = rand() % 100;
			if(rand_num < p2*100){ // stay at branch
				str_b.append(std::to_string(label_map[j])+",");
			}else{ // make new branch
				str_b.append("("+std::to_string(label_map[j])+",");
				num_branch++;
			}
		}

		str_b.pop_back();
		for(int j=0; j<num_branch; j++)
			str_b.append(")");
		str_b.append(";");

		std::ofstream writer(std::to_string(i)+"a.txt");
		if (writer.is_open()){
			writer << str_a;
			writer.close();
		}
		writer = std::ofstream(std::to_string(i)+"b.txt");
		if (writer.is_open()){
			writer << str_b;
			writer.close();
		}
	}
}
