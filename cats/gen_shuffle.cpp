#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <algorithm>
#include <vector>

// Usage: cat_gen [num_trees] [num_leaves] [branching_factor]
// branching_factor is a % value from 0 to 100,
// higher value means more leaves in each branch / lower tree height
int main(int argc, char* argv[]) {

	if(argc < 5){
		std::cout << "Usage: gen_shuffle [num_trees] [num_leaves] [p] [shuffle]" << std::endl;
		return 0;
	}

	srand (time(NULL));

	int num_trees = std::atoi(argv[1]);
        if(num_trees > 100) num_trees = 100;
	int num_leaves = std::atoi(argv[2]);
	double p = std::atof(argv[3]);
    double shuffle = std::atof(argv[4]);

	for(int i = 1; i <= num_trees; i++){
        std::vector<std::vector<int>> leaves;
        leaves.push_back(std::vector<int>());
        leaves[0].push_back(1);
        int depth = 0;
        int cur_leaf = 2;
        int rand_num;
        while(cur_leaf <= num_leaves){
            rand_num = rand() % 100;
            if(rand_num > p*100){ // make new branch
                leaves.push_back(std::vector<int>());
                depth++;
            }
            leaves[depth].push_back(cur_leaf++);
        }

        /* for(int j = 0; j < leaves.size(); j++){ */
        /*     for (int k = 0; k < leaves[j].size(); k++) { */
        /*         std::cout << leaves[j][k] << " "; */
        /*     } */
        /*     std::cout << std::endl; */
        /* } */
        // array to string
        std::string str("");
        for(int j = 0; j < leaves.size(); j++){
            str.append("(");
            for (int k = 0; k < leaves[j].size(); k++) {
				str.append(std::to_string(leaves[j][k]));
                if(leaves[j][k] < num_leaves)
                    str.append(",");
            }
        }
        for(int j = 0; j < leaves.size(); j++){
            str.append(")");
        }
        str.append(";");


		std::ofstream writer(std::to_string(i)+"a.txt");
		if (writer.is_open()){
			writer << str;
			writer.close();
		}
/* std::cout << "depth: " << depth << std::endl; */

        for (int d = 0; d <= depth; d++) {
            rand_num = rand() % 100;
            if(d < depth-1 && rand_num < shuffle*100){
                std::vector<int> tempvec;
                for(int num : leaves[d]){
                    tempvec.push_back(num);
                }
                for(int num : leaves[d+1]){
                    tempvec.push_back(num);
                }
                leaves[d].clear();
                leaves[d+1].clear();
                leaves[d+1].push_back(tempvec[0]);
                leaves[d].push_back(tempvec[1]);
                tempvec.erase(tempvec.begin(),tempvec.begin()+2);
                for(int num : tempvec){
                    rand_num = rand() % 100;
                    if(rand_num > 50){
                        leaves[d].push_back(num);
                    }else{
                        leaves[d+1].push_back(num);
                    }
                }
            }
        }

        /* for (int d = 0; d <= depth; d++) { */
        /*     if(leaves[d].empty()) */
        /*         leaves.erase(leaves.begin()+d); */
        /* } */

        /* for(int j = 0; j < leaves.size(); j++){ */
        /*     for (int k = 0; k < leaves[j].size(); k++) { */
        /*         std::cout << leaves[j][k] << " "; */
        /*     } */
        /*     std::cout << std::endl; */
        /* } */
        // array to string
        str = "";
        for(int j = 0; j < leaves.size(); j++){
            str.append("(");
            for (int k = 0; k < leaves[j].size(); k++) {
                str.append(std::to_string(leaves[j][k]));
                if(leaves[j][k] < num_leaves)
                    str.append(",");
            }
        }
        for(int j = 0; j < leaves.size(); j++){
            str.append(")");
        }
        str.append(";");


        writer = std::ofstream (std::to_string(i)+"b.txt");
        if (writer.is_open()){
            writer << str;
            writer.close();
        }
        /* writer = std::ofstream(std::to_string(i)+"b.txt"); */
		/* if (writer.is_open()){ */
		/* 	writer << str_b; */
		/* 	writer.close(); */
		/* } */
	}
}
